## Sejam bem-vindos!

Esperamos que o seu caminho até aqui tenha sido agradável.

Sinta-se à vontade para explorar este ambiente que foi pensado para aproximar você das soluções que você precisa.

E quais seriam essas soluções?

 - Implementação de ferramentas de qualidade;
 - Técnicas de conservação de alimentos;
 - Desenvolvimento e melhoria de embalagens;
 - Implementação de sistema de rastreabilidade;
 - Avaliação da eficiência de processos;
 - Treinamentos (Boas práticas de fabricação, legislação, ...);
 - Entre outras.
