---
title: Refrigeração de alimentos
date: 2019-11-16
author: Mariana Costa
tags: ["Refrigeração", "Alimentos", "Conservação"]
---

A tecnologia de refrigeração é o método de conservação dominante quando se trata 
de produtos perecíveis, principalmente dos alimentos. Consiste na remoção de 
calor desses  materiais, seguida de sua permanência em baixas temperaturas, 
com o intuito de reduzir ou inativar o crescimento e desenvolvimento de 
microrganismos patogênicos e/ou deteriorantes e algumas enzimas que podem 
comprometer a qualidade original do alimento, prolongando a sua vida de 
prateleira (SILVA, 2018; SADHU, 2018; OLIVEIRA et al., 2015).

Trata-se de uma tecnologia empregada em diferentes operações na produção, 
transporte, manuseio, armazenamento e distribuição dos alimentos, sendo o 
componente mais importante do que chamamos cadeia do frio, que é justamente 
esse conjunto de etapas em que se faz uso do frio para preservação das 
características do alimento por mais tempo. Qualquer interrupção na cadeia 
impacta negativamente a qualidade, a segurança e a vida de prateleira dos 
produtos de interesse devido ao rápido crescimento microbiano (SILVA, 2018).

A principal aplicação é na produção de produtos de origem animal como carnes e 
derivados de carne, leites e derivados de leite, entre outros. São produtos com 
alto valor nutritivo e que carregam uma certa carga microbiana, dependendo do 
nível de processamento a que forem submetidos, que se expostos a temperaturas 
elevadas, podem ver-se comprometidos do ponto de vista sensorial e 
microbiológico (SILVA, 2018; SADHU, 2018).

Dentre os derivados de leite, os leites fermentados, preparados lácteos 
submetidos ao processo fermentativo, constituem um grupo de grande relevância 
quando se fala de cadeia do frio, pois a refrigeração está presente na cadeia 
leiteira desde o pós-ordenha até as gôndolas dos supermercados. Além disso, a 
refrigeração também é usada para cessar a fermentação, garantindo as 
características sensoriais de interesse para um determinado produto 
(Ordóñez et al., 2005; COSTA, 2018).

Figura: Perfil de temperatura de um alimento.

![Perfil de temperatura de um alimento](/img/perfil-de-resfriamento-alimento.png)

### Referências:

COSTA, M. A. Relatório de estágio supervisionado obrigatório (ESO): Dairy
Partners Americas Nordeste Produtos Alimentícios Ltda: vivências na garantia da
qualidade e no sistema de inspeção federal. Trabalho de ESO 
(Estágio Supervisionado Obrigatório: Curso de Engenharia de Alimentos) – 
Universidade Federal Rural de Pernambuco, Departamento de Engenharia de 
Alimentos, Garanhuns, BR – PE, 2018.

OLIVEIRA, E. S. F.; SOARES, M. V.; HECK, R. T.; OLIVEIRA, E. M. Dimensionamento
do Sistema Frigorífico de Câmaras-frias para Congelamento e Resfriamento de 
Carnes. Anais do VII Salão Internacional de Ensino, Pesquisa e Extensão – 
Universidade Federal do Pampa, 2015.

ORDÓÑEZ, J. A. et al. Tecnologia de Alimentos. Vol. 2. 
Porto Alegre: Artmed, 2005.

SADHU, S. P. Effect of cold chain interruptions on the shelf-life of fluid 
pasteurised skim Milk at the consumer stage. Brazilian Journal of Food 
Technology. Campinas, v.21, e2017064, 2018.

SILVA, A. Câmaras Frigoríficas – aplicação, tipos, cálculo de carga térmica e 
boas práticas de utilização visando a racionalização da energia elétrica. 
Ambiente Gelado, 2018. Disponível em: 
<http://www.ambientegelado.com.br/v51/index.php/artigos-tecnicos> 
Acesso: Nov, 2018.